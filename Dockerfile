# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM golang:1.18-alpine3.15 AS builder

WORKDIR /app
COPY . .

ENV CGO_ENABLED=0
RUN mkdir out && \
    go mod download && \
    go build -o ./out/bbbrecorderd ./cmd/bbbrecorderd && \
    go build -o ./out/cloudctl ./cmd/cloudctl


FROM alpine:3.15

ARG BBB_SECRET
ARG HETZNER_APIKEY
ARG PRIVATE_KEY
ARG USER_FSFE_PW

RUN apk --no-cache add doas terraform yq && \
    mkdir -m 0700 -p /app/log /app/state && \
    addgroup -g 911 -S app && adduser -u 911 -S -G app app && \
    echo "permit nopass root as app" > /etc/doas.d/doas.conf

COPY --from=builder /app/out/* /bin/
COPY conf.crypted.yml /app/conf.crypted.yml

# As /app/{log,state} are mounted as volumes after container creation, extended
# privileges are necessary to change the owner to the current limited user.
#
# yq is used to insert secrets from current environment variables in the running
# container, without writing credentials to any disk.
# NOTE: yq's CLI syntax will most likley change when bumping alpine.
#
# Finnally, the app user is used to execute bbbrecorderd.
RUN echo -e '#!/bin/sh                                        \n\
                                                              \n\
             chown -R app:app /app                            \n\
                                                              \n\
             CONF="$(mktemp)"                                 \n\
             yq e ".bbb.secret = strenv(BBB_SECRET) |         \n\
                   .hetzner_apikey = strenv(HETZNER_APIKEY) | \n\
                   .ssh.private_key = strenv(PRIVATE_KEY) |   \n\
                   .auth_users[0].pass = strenv(USER_FSFE_PW) \n\
               " /app/conf.crypted.yml > "$CONF"              \n\
             chown app:app "$CONF" && chmod 0400 "$CONF"      \n\
                                                              \n\
             doas -u app /bin/bbbrecorderd "$CONF"            \n\
      ' > /bin/startup.sh && \
    chmod +x /bin/startup.sh

CMD /bin/startup.sh

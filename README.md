<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# BBB Recorder
[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/bbb-recorder/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/bbb-recorder)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/bbb-recorder)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/bbb-recorder/status.svg?ref=refs/heads/main)](https://drone.fsfe.org/fsfe-system-hackers/bbb-recorder)

Web service to record BigBlueButton meetings as a video file in one click, more or less.


## Architecture

The BBB Recorder, `bbbrecorderd`, serves a web application which internally connects multiple APIs.
This complexity is hidden from the user.
From a user's perspective, a current BigBlueButton (_BBB_) meeting will be selected and the recording starts automatically.

However, the BBB Recorder queries a configured BBB server through its API to fetch those meetings.
After a user starts a recording session, a new virtual server is created at Hetzner Cloud and automatically provisioned to join and record this meeting.

When the session should be stopped, the BBB Recorder stops the recording service on the VM via SSH.
Afterwards, the recorded video file will automatically be copied to a file server via `scp`, Agent Forwarding is possible.


## Installation

To build the software, a current Go compiler is necessary.
Furthermore, Terraform needs to be available in the `$PATH` of the system where `bbbrecorderd` will be executed.

As it's currently open how the software should be deployed, this section needs an update later on.

However, to get a working environment, one can enter a `nix-shell`.

```
go build ./cmd/bbbrecorderd
```


## Deployment

The application will be deployed through Docker Compose by Drone.
As the configuration stores confidential data, those values have placeholders in the configuration.
During container image creation, those values will be substituted by Drone secrets.
For details, please take a look at `.drone.yml`, `Dockerfile`, and `docker-compose.yml`.


## Configuration

The whole configuration is passed through a YAML file, passed as `bbbrecorderd`'s only argument.
Please start with taking a look at `conf.crypted.yml`, which uses placeholders where secrets would be.
For more details, please refer to the Deployment section.

The keys have the following meaning:

- `bbb`: Section for BBB API access.
    - `url`: API endpoint of the BBB server, MUST end with a trailing slash.
    - `secret`: API secret, can be fetched on the BBB server via `bbb-conf --secret`.
- `hetzner_apikey`: Hetzner Cloud API token with _Read_ and _Write_ permissions.
- `ssh`: Section for SSH access.
    - `private_key`: Matching private key, used for authentication.
      This key's public key should be stored at Hetzner Cloud and be listed in the following `public_key_names`.
      If `scp` is used with Agent Forwarding, this private key's public key should also be present on the fileserver.
    - `public_key_names`: List of names of SSH public keys in the Hetzner web interface.
- `state_dir`: Directory where `bbbrecorderd` stores its state.
- `log_file`: Log file for `bbbrecorderd`, also useful for auditing.
- `up_commands` and `down_commands`: List of commands to be executed via SSH on the VM during creation or destruction.
  Each entry might have up to three fields:
    - `cmd`: Command to be executed on the VM.
    - `timeout`: Maximum duration for this command, syntax of [Golang's `time.ParseDuration`](https://pkg.go.dev/time#ParseDuration).
    - `agent_forwarding`: Boolean value, forwards the SSH Agent when true.
      This might be usefull to execute commands on the VM with access to your SSH private key, e.g., for `scp`.
- `auth_users`: List of user credentials for web access.
  Each entry needs those two fields configured:
    - `name`: Username.
    - `pass`: Password in plaintext.


## Usage

The web panel has two tabs, _Sessions_ and _Logs_, which can be switched on the top right corner.

In the _Sessions_ view, one can see all currently active recording sessions as well as previous ones.
Each session has a creation and destruction event, where the latter is missing at running sessions.
Those two events are linked each to an request with a human readable name and have a starting and finishing timestamp.
Active sessions can be stopped through the button under its information table.

New sessions can be created through the big button at the top, _Create a new Session_.
When clicked, the user is asked to select a meeting and confirm the action.

All those actions are being logged and can be viewed from the _Logs_ tab.
This view shows a table of log events, where each web request creates an unique _Request ID_ which is being referred in all related log entries.
This allows linking log entries to events which take some time, e.g., creating or destroying a session.

As each API request creates a log entry, there are lots of them, e.g., for accessing the logs.
Thus, by default those simple GET requests are filtered in the web log view, but can be shown by switching the _Filter GET request messages_ switch.

Furthermore, all log files will be written to a file, allowing further inspections.

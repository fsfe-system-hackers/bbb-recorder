// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Package bbb implements the necessary API calls to the BigBlueButton API.
package bbb

import (
	"context"
	"crypto/sha1"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
)

// Api supports a limited subset of BigBlueButton's API to fetch available
// meetings to be recorded. The struct fields, URL and Secret, should be
// populated due to the output of `bbb-conf --secret`. The URL might be, e.g.,
// "https://bbb.hsmr.cc/bigbluebutton/". It MUST end with a slash and MUST NOT
// end with "api/".
type Api struct {
	Url    string
	Secret string
}

// sharedSecret to be used as the MAC for API calls.
func (api Api) sharedSecret(apiName, query string) string {
	secHash := sha1.New()
	_, _ = io.WriteString(secHash, apiName)
	_, _ = io.WriteString(secHash, query)
	_, _ = io.WriteString(secHash, api.Secret)
	return fmt.Sprintf("%x", secHash.Sum(nil))
}

// apiMeetingResp is used internally within GetMeetings to be populated by the
// API's XML response.
type apiMeetingResp struct {
	XMLName    xml.Name `xml:"response"`
	ReturnCode string   `xml:"returncode"`
	Meetings   []struct {
		MeetingName string `xml:"meetingName"`
		MeetingId   string `xml:"meetingID"`
	} `xml:"meetings>meeting"`
}

// fetchMeetings from the BBB API. This method is called from GetMeetings which
// handlex contexts.
func (api Api) fetchMeetings() (map[string]string, error) {
	reqUrl := fmt.Sprintf("%sapi/getMeetings?checksum=%s",
		api.Url, api.sharedSecret("getMeetings", ""))

	resp, err := http.Get(reqUrl)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected HTTP status code %d", resp.StatusCode)
	}

	apiResp := apiMeetingResp{}
	err = xml.NewDecoder(resp.Body).Decode(&apiResp)
	if err != nil {
		return nil, err
	}

	if apiResp.ReturnCode != "SUCCESS" {
		return nil, fmt.Errorf("API call's return code is %q", apiResp.ReturnCode)
	}

	meetings := make(map[string]string)
	for _, meeting := range apiResp.Meetings {
		meetings[meeting.MeetingName] = meeting.MeetingId
	}
	return meetings, nil
}

// GetMeetings queries all current meetings, returning a map of the meeting's
// name to its ID.
func (api Api) GetMeetings(ctx context.Context) (meetings map[string]string, err error) {
	fin := make(chan struct{}, 1)

	go func() {
		meetings, err = api.fetchMeetings()
		close(fin)
	}()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()

	case <-fin:
		return
	}
}

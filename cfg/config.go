// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Package cfg defines a configuration format across all cmds.
package cfg

import (
	"fmt"
	"time"

	"gopkg.in/yaml.v3"
)

// Command will be executed on the remote machine via SSH. Each command is
// defined by the Cmd to be executed, a Timeout[0], and optionally
// AgentForwarding.
//
//   [0] https://pkg.go.dev/time#ParseDuration
//
type Command struct {
	Cmd             string
	Timeout         time.Duration
	AgentForwarding bool `yaml:"agent_forwarding"`
}

func (command Command) String() string {
	return fmt.Sprintf("%q (timeout: %v, agent forwarding: %t)",
		command.Cmd, command.Timeout, command.AgentForwarding)
}

// AuthUser is an allowed user for HTTP access.
type AuthUser struct {
	Name string
	Pass string
}

func (authUser AuthUser) String() string {
	return authUser.Name
}

// Config for the cmds. Some fields might not be necessary for each cmd, as
// hopefully properly documented.
type Config struct {
	Bbb struct {
		Url    string
		Secret string
	}

	HetznerCloudApiKey string `yaml:"hetzner_apikey"`

	Ssh struct {
		PrivateKey     string   `yaml:"private_key"`
		PublicKeyNames []string `yaml:"public_key_names"`
	}

	StateDir string `yaml:"state_dir"`
	LogFile  string `yaml:"log_file"`

	UpCommands   []Command `yaml:"up_commands"`
	DownCommands []Command `yaml:"down_commands"`

	AuthUsers []AuthUser `yaml:"auth_users"`
}

// Parse a Config.
func Parse(in []byte) (conf Config, err error) {
	err = yaml.Unmarshal(in, &conf)
	return
}

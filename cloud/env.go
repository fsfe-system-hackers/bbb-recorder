// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file contains code to prepare an environment / a directory with all
// necessary files to run Terraform within.

package cloud

import (
	"bytes"
	"embed"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/sethvargo/go-diceware/diceware"
)

//go:embed inc/*
var envTemplateFs embed.FS

var envTemplate *template.Template

func init() {
	var err error
	envTemplate, err = template.New("env").ParseFS(envTemplateFs, "inc/*")
	if err != nil {
		panic(err)
	}
}

// Environment to be created for Terraform usage.
type Environment struct {
	// Those values MUST be provided.
	HetznerCloudApiKey string
	SshPublicKeyNames  []string
	BbbUrl             string
	BbbSecret          string
	BbbMeetingId       string

	// The following value will be populated.
	Dir string

	// The following values will be populated and MUST NOT be set.
	Hostname      string
	DockerCompose string
	ConvertMp4    string
}

// populateBase64Var loads base64 encoded output of a template into a variable.
func (env *Environment) populateBase64Var(name string, field *string) error {
	var buff bytes.Buffer
	err := envTemplate.ExecuteTemplate(&buff, name, env)
	if err != nil {
		return err
	}

	*field = base64.StdEncoding.EncodeToString(buff.Bytes())
	return nil
}

// populateDockerCompose base64 encoded into the struct's DockerCompose field.
func (env *Environment) populateDockerCompose() error {
	return env.populateBase64Var("docker-compose.yml", &env.DockerCompose)
}

// populateConvertMp4 base64 encoded into the struct's ConvertMp4 field.
func (env *Environment) populateConvertMp4() error {
	return env.populateBase64Var("convert_mp4.sh", &env.ConvertMp4)
}

// populateFile as specified into the directory.
func (env *Environment) populateFile(filename string) error {
	f, err := os.Create(env.Dir + "/" + filename)
	if err != nil {
		return err
	}

	err = f.Chmod(00600)
	if err != nil {
		return err
	}

	err = envTemplate.ExecuteTemplate(f, filename, env)
	if err != nil {
		return err
	}

	return f.Close()
}

// populateCloudInit into the Environment's directory.
func (env *Environment) populateCloudInit() error {
	return env.populateFile("cloud_init.yml")
}

// populateMainTf, Terraform's main.tf file, into the Environment's directory.
func (env *Environment) populateMainTf() error {
	hostnameParts, err := diceware.GenerateWithWordList(3, diceware.WordListEffSmall())
	if err != nil {
		return err
	}
	env.Hostname = strings.Join(hostnameParts, "")

	return env.populateFile("main.tf")
}

// Populate an Environment into a new directory under the given root directory.
// Afterwards, one can `cd` into this directory and use Terraform.
func (env *Environment) Populate(rootDir string) (dir string, err error) {
	randomParts, err := diceware.GenerateWithWordList(4, diceware.WordListEffSmall())
	if err != nil {
		return
	}
	env.Dir = filepath.Join(rootDir, "tf_"+strings.Join(randomParts, "_"))

	err = os.Mkdir(env.Dir, 00700)
	if err != nil {
		return
	}

	defer func() {
		if err != nil {
			env.Dir = ""
		}
		dir = env.Dir
	}()

	populators := []func() error{
		env.populateDockerCompose,
		env.populateConvertMp4,
		env.populateCloudInit,
		env.populateMainTf,
	}
	for _, populator := range populators {
		err = populator()
		if err != nil {
			return
		}
	}

	return
}

// Terraform controller to orchestrate the created Environment.
func (env *Environment) Terraform() (*Terraform, error) {
	if env.Dir == "" {
		return nil, fmt.Errorf("Environment was not `Populate`d")
	}

	dir, err := filepath.Abs(env.Dir)
	if err != nil {
		return nil, err
	}

	return &Terraform{Dir: dir}, nil
}

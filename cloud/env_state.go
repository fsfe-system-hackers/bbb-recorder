// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file contains code to describe an Environment's state.

package cloud

import (
	"encoding/json"
	"errors"
	"os"
	"path/filepath"
	"time"
)

const stateFileName = "env_state.json"

// EnvironmentState will be serialized to a JSON file in the Environment's
// directory for information about the Environment.
type EnvironmentState struct {
	Dir string `json:"-"`

	// Creator is the creating job's name. CreatedStart and CreatedFinished are
	// timestamps when the job started and finished.
	Creator         string    `json:"creator"`
	CreatedStart    time.Time `json:"created_start"`
	CreatedFinished time.Time `json:"created_finished"`

	// Destroyer is the destroying job's name. DestroyedStart and
	// DestroyedFinished are timestamps when the job started and finished.
	Destroyer         string    `json:"destroyer"`
	DestroyedStart    time.Time `json:"destroyed_start"`
	DestroyedFinished time.Time `json:"destroyed_finished"`

	// Error message in case something went wrong.
	Error string `json:"error,omitempty"`

	// MetaData might store further information to be displayed.
	MetaData map[string]string `json:"meta_data,omitempty"`
}

// NewEnvironmentState returns an EnvironmentState for an Environment based on
// its directory. If no EnvironmentState exists, a new one will be created.
func NewEnvironmentState(dir string) (*EnvironmentState, error) {
	state := &EnvironmentState{
		Dir:      dir,
		MetaData: make(map[string]string),
	}
	_, stateExistsErr := os.Stat(state.stateFile())

	switch {
	case errors.Is(stateExistsErr, os.ErrNotExist):
		return state, state.Write()

	case stateExistsErr == nil:
		f, err := os.Open(state.stateFile())
		if err != nil {
			return nil, err
		}

		defer f.Close()

		return state, json.NewDecoder(f).Decode(state)

	default:
		return nil, stateExistsErr
	}
}

func (state *EnvironmentState) stateFile() string {
	return filepath.Join(state.Dir, stateFileName)
}

// Write the current EnvironmentState to its file.
func (state *EnvironmentState) Write() error {
	f, err := os.Create(state.stateFile())
	if err != nil {
		return err
	}

	defer f.Close()

	return json.NewEncoder(f).Encode(state)
}

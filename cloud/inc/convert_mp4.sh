#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

pushd /bbb_livestreaming
F="$(basename ./video/meeting-*.mkv .mkv)"
docker-compose run bbb_livestreaming ffmpeg -i "/video/${F}.mkv" -c copy -movflags faststart "/video/${F}.mp4"
popd

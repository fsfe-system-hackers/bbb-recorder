# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.33"
    }
  }
}

provider "hcloud" {
  token = "{{.HetznerCloudApiKey}}"
}

resource "hcloud_server" "recording_host" {
  name        = "{{.Hostname}}"
  server_type = "ccx22"
  image       = "debian-11"
  location    = "nbg1"
  user_data   = file("./cloud_init.yml")
  ssh_keys    = [
    {{- range $sshPublicKeyName :=  .SshPublicKeyNames }}
      {{ $sshPublicKeyName | printf "%q," }}
    {{- end }}
  ]
}

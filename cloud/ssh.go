// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file contains code to interact with the VM via SSH.

package cloud

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

const tofuKeyFilename = "tofu_known_host"

// SshHandler for a Terraform environment within a directory and the host's
// private key to be used for the root login.
type SshHandler struct {
	Dir           string
	SshPrivateKey string
}

// tofuHostKeyCallback implements the ssh.HostKeyCallback in a TOFU, trust on
// first use, matter. After the first connection, the host' public key will be
// serialized into the environment directory.
func (handler *SshHandler) tofuHostKeyCallback(hostname string, _ net.Addr, key ssh.PublicKey) error {
	tofuFile := filepath.Join(handler.Dir, tofuKeyFilename)
	if _, statErr := os.Stat(tofuFile); errors.Is(statErr, os.ErrNotExist) {
		return ioutil.WriteFile(tofuFile, key.Marshal(), 0644)
	}

	tofuKey, err := ioutil.ReadFile(tofuFile)
	if err != nil {
		return err
	}

	if !bytes.Equal(tofuKey, key.Marshal()) {
		return fmt.Errorf("public key differs from stored key in %s", tofuFile)
	}
	return nil
}

// connect to the VM created by Terraform via SSH. Both versions of the Internet
// Protocol are tried before giving up.
func (handler *SshHandler) connect(ctx context.Context) (*ssh.Client, agent.Agent, error) {
	tfStatus, err := (&Terraform{Dir: handler.Dir}).Status(ctx)
	if err != nil {
		return nil, nil, err
	}

	privKeyRaw, err := ssh.ParseRawPrivateKey([]byte(handler.SshPrivateKey))
	if err != nil {
		return nil, nil, err
	}

	clientAgent := agent.NewKeyring()
	err = clientAgent.Add(agent.AddedKey{PrivateKey: privKeyRaw})
	if err != nil {
		return nil, nil, err
	}

	clientCfg := &ssh.ClientConfig{
		User:            "root",
		Auth:            []ssh.AuthMethod{ssh.PublicKeysCallback(clientAgent.Signers)},
		HostKeyCallback: handler.tofuHostKeyCallback,
		Timeout:         time.Minute,
	}

	feedbackCh := make(chan interface{}, 1)

	go func() {
		remotes := []string{
			"[" + tfStatus.Addr6.String() + "]",
			tfStatus.Addr4.String(),
		}

		var errs error
		for _, remote := range remotes {
			client, err := ssh.Dial("tcp", remote+":22", clientCfg)
			if err != nil {
				errs = fmt.Errorf("%v, %v", err, errs)
				continue
			}

			feedbackCh <- client
			return
		}

		feedbackCh <- errs
	}()

	select {
	case <-ctx.Done():
		return nil, nil, ctx.Err()

	case feedback := <-feedbackCh:
		switch feedback := feedback.(type) {
		case *ssh.Client:
			return feedback, clientAgent, nil
		case error:
			return nil, nil, feedback
		default:
			return nil, nil, fmt.Errorf("unexpected feedback %q", feedback)
		}
	}
}

// Exec a remote command and return the combined output. This method establishes
// a SSH connection, session and handles its tear down afterwards.
func (handler *SshHandler) Exec(ctx context.Context, cmd string, agentFwd bool) (out []byte, err error) {
	client, clientAgent, clientErr := handler.connect(ctx)
	if clientErr != nil {
		err = clientErr
		return
	}

	defer client.Close()

	fin := make(chan struct{}, 1)

	go func() {
		defer close(fin)

		session, sessionErr := client.NewSession()
		if sessionErr != nil {
			err = sessionErr
			return
		}

		if agentFwd {
			err = agent.ForwardToAgent(client, clientAgent)
			if err != nil {
				return
			}

			err = agent.RequestAgentForwarding(session)
			if err != nil {
				return
			}
		}

		out, err = session.CombinedOutput(cmd)
	}()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()

	case <-fin:
		return
	}
}

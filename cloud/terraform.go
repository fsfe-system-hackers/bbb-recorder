// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file contains code to run Terraform within a previously created
// environment.

package cloud

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os/exec"
	"strings"
)

// Terraform controller to be executed within a directory. An instance an be
// created via the Environment.Terraform method.
type Terraform struct {
	Dir string
}

// cmd executes a Terraform command and returns the combined output of stdout
// and stderr.
func (tf *Terraform) cmd(ctx context.Context, args ...string) ([]string, error) {
	tfCmd := exec.CommandContext(ctx, "terraform", args...)
	tfCmd.Dir = tf.Dir

	out, err := tfCmd.CombinedOutput()
	return strings.Split(string(out), "\n"), err
}

// Init the configuration. See `terraform init`.
func (tf *Terraform) Init(ctx context.Context) ([]string, error) {
	return tf.cmd(ctx, "init", "-input=false", "-no-color")
}

// Plan the configuration. See `terraform plan`.
func (tf *Terraform) Plan(ctx context.Context) ([]string, error) {
	return tf.cmd(ctx, "plan", "-input=false", "-no-color", "-out=tfplan")
}

// Apply the configuration. See `terraform apply`.
func (tf *Terraform) Apply(ctx context.Context) ([]string, error) {
	return tf.cmd(ctx, "apply", "-auto-approve", "-input=false", "-no-color", "tfplan")
}

// Destroy the configuration. See `terraform destroy`.
func (tf *Terraform) Destroy(ctx context.Context) ([]string, error) {
	return tf.cmd(ctx, "destroy", "-auto-approve", "-input=false", "-no-color")
}

// TerraformStatus represents a subset of the  VM's status from Terraform.
type TerraformStatus struct {
	Name  string
	Addr4 net.IP
	Addr6 net.IP
}

// Status of the provisioned VM resource.
func (tf *Terraform) Status(ctx context.Context) (ts TerraformStatus, err error) {
	out, outErr := tf.cmd(ctx, "show", "-no-color", "-json")
	if outErr != nil {
		err = outErr
		return
	}

	var outData map[string]interface{}
	err = json.Unmarshal([]byte(strings.Join(out, "")), &outData)
	if err != nil {
		return
	}

	// The following code just assumes that the JSON is in the expected order.
	// That's what the recover is for, in case that it isn't. Also, it's ugly.

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("recovered from: %v", r)
		}
	}()

	values := outData["values"].(map[string]interface{})
	rootModules := values["root_module"].(map[string]interface{})
	resources := rootModules["resources"].([]interface{})

	for _, resourceIf := range resources {
		resource := resourceIf.(map[string]interface{})
		if resource["type"] != "hcloud_server" {
			continue
		}

		v := resource["values"].(map[string]interface{})

		if status := (v["status"].(string)); status != "running" {
			err = fmt.Errorf("status is %q instead of \"running\"", status)
			return
		}

		ts.Name = v["name"].(string)
		ts.Addr4 = net.ParseIP(v["ipv4_address"].(string))
		ts.Addr6 = net.ParseIP(v["ipv6_address"].(string))
	}

	if ts.Name == "" {
		err = fmt.Errorf("found no hcloud_server")
	}
	return
}

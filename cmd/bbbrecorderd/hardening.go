// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//go:build !linux
// +build !linux

package main

// hardening will active some platform-specific hardening.
func hardening() {
}

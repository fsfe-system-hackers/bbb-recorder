// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"strings"

	syscallset "github.com/oxzi/syscallset-go"
)

// hardening activates a seccomp-bpf filter.
func hardening() {
	if !syscallset.IsSupported() {
		return
	}

	filter := []string{
		"@system-service",
		"~@clock",
		"~@cpu-emulation",
		"~@keyring",
		"~@memlock",
		"~@module",
		"~@mount",
		"~@privileged",
		"~@reboot",
		"~@setuid",
		"~@swap",
	}

	err := syscallset.LimitTo(strings.Join(filter, " "))
	if err != nil {
		panic(err)
	}
}

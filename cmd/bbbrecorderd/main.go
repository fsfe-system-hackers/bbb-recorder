// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"embed"
	"fmt"
	"io/fs"
	"io/ioutil"
	"net/http"
	"os"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cfg"
	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/webapi"
)

//go:embed webstatic
var webstatic embed.FS

// init currently starts dropping privileges before executing the main function.
func init() {
	hardening()
}

// parseConfig from a file.
func parseConfig(configFile string) (cfg.Config, error) {
	configData, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		return cfg.Config{}, err
	}

	return cfg.Parse(configData)
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s conf.yml\n", os.Args[0])
		os.Exit(1)
	}

	config, err := parseConfig(os.Args[1])
	if err != nil {
		panic(err)
	}

	apiHandler, err := webapi.NewApi(config)
	if err != nil {
		panic(err)
	}

	// When working on the frontend code, one might wanna change the following
	// code block by the following line. This results in reading the files live
	// from disk instead of embedding all static files into the executable.
	//
	// staticFilesHandler := http.FileServer(http.Dir("./cmd/bbbrecorderd/webstatic"))

	staticFs, err := fs.Sub(webstatic, "webstatic")
	if err != nil {
		panic(err)
	}
	staticFilesHandler := http.FileServer(http.FS(staticFs))

	http.Handle("/", staticFilesHandler)
	http.Handle("/api/", apiHandler)

	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// uiBbbMeetingsList fetches all current BBB meetings and updates the UI.
const uiBbbMeetingsList = async () => {
  const meetingOpts = document.querySelector('#bbb-meeting-id');
  meetingOpts.innerHTML = '';

  const meetings = await fetch('/api/bbb/meetings').
    then(resp => resp.json()).
    then(resp => resp.meetings);

  Object.keys(meetings).forEach(meetingName => {
    const meetingOpt = document.createElement('option');
    meetingOpt.value = meetings[meetingName];
    meetingOpt.text = meetingName;

    meetingOpts.append(meetingOpt);
  });
};

// uiSessionList fetches all current sessions and updates the UI.
const uiSessionList = async () => {
  const templ = document.querySelector('#session-tmpl');
  const sessionsActiveDiv = document.createElement('div');
  const sessionsPreviousDiv = document.createElement('div');

  const fmtTimestamp = (ts) => {
    return (ts == '0001-01-01T00:00:00Z') ? '-' : ts;
  };

  const sessions = await fetch('/api/session/list').
    then(resp => resp.json()).
    then(resp => resp.sessions);

  Object.keys(sessions).forEach(sessionName => {
    const session = sessions[sessionName];
    const sessionObj = templ.content.cloneNode(true);

    sessionObj.querySelector('#session-tmpl-name').textContent = sessionName;

    if (session.hasOwnProperty('error')) {
      const errorMsg = sessionObj.querySelector('#session-tmpl-error');
      errorMsg.removeAttribute('hidden');

      const errorMark = document.createElement('mark');
      errorMark.textContent = 'Error';
      const errorText = document.createTextNode(session.error);

      errorMsg.appendChild(errorMark);
      errorMsg.appendChild(errorText);
    }

    if (session.hasOwnProperty('meta_data')) {
      const metaList = sessionObj.querySelector('#session-tmpl-meta');
      metaList.removeAttribute('hidden');

      Object.keys(session.meta_data).forEach(metaKey => {
        const metaValue = session.meta_data[metaKey];

        const metaKeyObj = document.createTextNode(`${metaKey}: `);
        const metaValueObj = document.createElement('i');
        metaValueObj.textContent = metaValue;

        const metaLi = document.createElement('li');
        metaLi.appendChild(metaKeyObj);
        metaLi.appendChild(metaValueObj);

        metaList.appendChild(metaLi);
      });
    }

    const creationTd = sessionObj.querySelectorAll('#session-tmpl-tbl-creation > td');
    creationTd[0].textContent = session.creator;
    creationTd[1].textContent = fmtTimestamp(session.created_start);
    creationTd[2].textContent = fmtTimestamp(session.created_finished);

    const destructionTd = sessionObj.querySelectorAll('#session-tmpl-tbl-destruction > td');
    destructionTd[0].textContent = session.destroyer;
    destructionTd[1].textContent = fmtTimestamp(session.destroyed_start);
    destructionTd[2].textContent = fmtTimestamp(session.destroyed_finished);

    const noDestructionStart = destructionTd[1].textContent == '-';
    const errorAndNoDestructionFin = session.hasOwnProperty('error') && destructionTd[2].textContent == '-';
    if (noDestructionStart || errorAndNoDestructionFin) {
      const stopBtn = sessionObj.querySelector('#session-tmpl-stop');
      stopBtn.removeAttribute('hidden');

      if (noDestructionStart) {
        stopBtn.setAttribute('onClick', `uiSessionDestroyPrepare('${sessionName}', false); toggleModal(event)`);
      } else if (errorAndNoDestructionFin) {
        stopBtn.innerHTML = `Force ${stopBtn.innerHTML}`;
        stopBtn.setAttribute('onClick', `uiSessionDestroyPrepare('${sessionName}', true); toggleModal(event)`);
      }
    }

    ((destructionTd[2].textContent == '-') ? sessionsActiveDiv : sessionsPreviousDiv).appendChild(sessionObj);
  });

  const activeSessions = document.querySelector('#active-sessions > div');
  activeSessions.innerHTML = sessionsActiveDiv.innerHTML;

  const previousSessions = document.querySelector('#previous-sessions > div');
  previousSessions.innerHTML = sessionsPreviousDiv.innerHTML;
};

// uiSessionNew creates a new session based on the current UI settings.
const uiSessionNew = async () => {
  const bbbMeetingId = document.querySelector('#bbb-meeting-id').value;

  await fetch('/api/session/new', {
      method: 'POST',
      body: JSON.stringify({'bbb_meeting_id': bbbMeetingId})
    }).
    then(resp => resp.json()).
    then(resp => {
      if (resp.hasOwnProperty('error')) {
        console.log(resp.error);
        alert(resp.error);
      }
    });

  uiSessionList();
};

// uiSessionDestroyPrepare writes the session ID into the modal for further
// usage in uiSessionDestroy.
const uiSessionDestroyPrepare = async (sessionId, forceDestruction) => {
  const modalTitle = document.querySelector('#session-stop-sessionid');
  modalTitle.textContent = sessionId;

  const inputForce = document.querySelector('#session-stop-force');
  inputForce.setAttribute('value', forceDestruction);
};

// uiSessionDestroy stops/destroys a session.
const uiSessionDestroy = async () => {
  const sessionId = document.querySelector('#session-stop-sessionid').textContent;
  const inputForce = document.querySelector('#session-stop-force').getAttribute('value');

  await fetch('/api/session/destroy', {
      method: 'POST',
      body: JSON.stringify({
        'session': sessionId,
        'force': JSON.parse(inputForce.toLowerCase())
      })
    }).
    then(resp => resp.json()).
    then(resp => {
      if (resp.hasOwnProperty('error')) {
        console.log(resp.error);
        alert(resp.error);
      }
    });

  uiSessionList();
};

// uiLogs fetches the server's log and displays it within the UI.
const uiLogs = async () => {
  const templ = document.querySelector('#log-tmpl');
  const filterNoGet = document.querySelector('#log-switch-filter').checked;

  const logTbody = document.querySelector('#log-tbody');
  const logTimestamps = logTbody.querySelectorAll('#log-timestamp');
  const since = (logTimestamps.length == 0) ? '' : logTimestamps[logTimestamps.length-1].textContent;

  const reqUrl = '/api/logs?' + new URLSearchParams({
    'since': since,
    'no-get': filterNoGet
  });
  const logEntries = await fetch(reqUrl).then(resp => resp.json());
  logEntries.forEach(logEntry => {
    const logTr = templ.content.cloneNode(true);

    const logTd = logTr.querySelectorAll('td');
    logTd[0].textContent = logEntry.Timestamp;
    logTd[1].textContent = logEntry.RequestId;
    logTd[2].textContent = logEntry.Message;

    logTbody.appendChild(logTr);
  });
};

// uiLogFilterSwitch refreshes the log table after enabling or disabling log
// filtering.
const uiLogFilterSwitch = async () => {
  const logTbody = document.querySelector('#log-tbody');
  logTbody.innerHTML = '';

  uiLogs();
};

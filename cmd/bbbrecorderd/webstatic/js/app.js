// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// intervalWorker holds the current setInterval's id to be canceled.
let intervalWorker = 0;

const loadNav = (activeView) => {
  const worker = {
    session: workerSession,
    log: workerLog
  };

  Object.keys(worker).forEach(view => {
    const btn = document.querySelector(`#${view}-btn`);
    if (view == activeView) {
      btn.setAttribute('class', 'secondary');
    } else {
      btn.setAttribute('class', 'secondary outline');
    }

    const div = document.querySelector(`#${view}-tab`);
    if (view == activeView) {
      div.removeAttribute('hidden');
    } else {
      div.setAttribute('hidden', true);
    }
  });

  if (intervalWorker != 0) {
    clearInterval(intervalWorker);
  }
  intervalWorker = setInterval(worker[activeView], 10000);

  worker[activeView]();
};

// workerSession is the background job for sessions.
const workerSession = () => {
  uiBbbMeetingsList();
  uiSessionList();
};

// workerLog is the background job for logs.
const workerLog = () => {
  uiLogs();
};

loadNav('session');

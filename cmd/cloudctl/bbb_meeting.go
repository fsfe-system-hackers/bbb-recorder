// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Query current meetings from BigBlueButton's API.

package main

import (
	"context"
	"fmt"
	"time"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/bbb"
)

func bbbMeetingMain() {
	bbbApi := bbb.Api{
		Url:    config.Bbb.Url,
		Secret: config.Bbb.Secret,
	}

	ctx, ctxCancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer ctxCancel()

	meetings, err := bbbApi.GetMeetings(ctx)
	if err != nil {
		panic(err)
	}

	fmt.Println("# Active Meetings")

	if len(meetings) == 0 {
		fmt.Println("There are currently no active meetings.")
	} else {
		for meetingName, meetingId := range meetings {
			fmt.Printf("- %q -> %q\n", meetingName, meetingId)
		}
	}
}

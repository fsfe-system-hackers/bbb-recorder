// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// destroy an environment again. This command just wraps `terraform destroy`.

package main

import (
	"context"
	"fmt"
	"time"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cloud"
)

func destroyMain() {
	tf := &cloud.Terraform{
		Dir: flagDir,
	}

	ctx, ctxCancel := context.WithTimeout(context.Background(), time.Minute)
	defer ctxCancel()

	out, err := tf.Destroy(ctx)
	for _, l := range out {
		fmt.Printf("> %s\n", l)
	}
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nDestroyed environment in %s\n", tf.Dir)
	fmt.Printf("You might wanna delete this directory now.\n")
}

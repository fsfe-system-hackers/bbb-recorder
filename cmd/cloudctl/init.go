// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// init a new environment in a new directory, which will be created. This
// environment will directly be applied. Thus, the VM should boot up directly.

package main

import (
	"context"
	"fmt"
	"time"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cloud"
)

func initMain() {
	if flagInitBbbMeeting == "" {
		panic("-bbb-meeting flag is missing or empty")
	}

	env := &cloud.Environment{
		HetznerCloudApiKey: config.HetznerCloudApiKey,
		SshPublicKeyNames:  config.Ssh.PublicKeyNames,
		BbbUrl:             config.Bbb.Url,
		BbbSecret:          config.Bbb.Secret,
		BbbMeetingId:       flagInitBbbMeeting,
	}

	dir, err := env.Populate(".")
	if err != nil {
		panic(err)
	}
	fmt.Printf("Created new environment in %s\n", dir)

	tf, err := env.Terraform()
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	steps := []struct {
		name string
		f    func(context.Context) ([]string, error)
	}{
		{"init ", tf.Init},
		{"plan ", tf.Plan},
		{"apply", tf.Apply},
	}

	for _, step := range steps {
		fmt.Printf("Starting Terraform %s\n", step.name)

		stepCtx, stepCtxCancel := context.WithTimeout(ctx, time.Minute)
		out, err := step.f(stepCtx)
		for _, l := range out {
			fmt.Printf("[%s] > %s\n", step.name, l)
		}
		if err != nil {
			panic(err)
		}

		stepCtxCancel()
	}

	fmt.Printf("\nEnvironment successfully created in %s\n", dir)
}

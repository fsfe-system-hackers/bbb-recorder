// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// The cloudctl cmd helps setting up, controlling and tearing down VMs for BBB
// recording automatically.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cfg"
)

var (
	// config file parsed, passed through -config flag.
	config cfg.Config

	// flagConfig is the path of the YAML config; used in most subcommands.
	flagConfig string

	// flagDir is the environment's directory; used in most subcommands.
	flagDir string

	// flagInitBbbMeeting is the BBB meeting ID to be recorded.
	flagInitBbbMeeting string

	// flagSshCmd is the remote command to be executed; ssh subcommand.
	flagSshCmd string

	// flagSshAgentForwarding enables SSH Agent Forwarding; ssh subcommand.
	flagSshAgentForwarding bool
)

func main() {
	initFlags := flag.NewFlagSet("init", flag.ExitOnError)
	initFlags.StringVar(&flagConfig, "config", "", "YAML configuration")
	initFlags.StringVar(&flagInitBbbMeeting, "bbb-meeting", "", "BBB Meeting ID")

	sshFlags := flag.NewFlagSet("ssh", flag.ExitOnError)
	sshFlags.StringVar(&flagConfig, "config", "", "YAML configuration")
	sshFlags.StringVar(&flagDir, "dir", "", "Environment directory")
	sshFlags.StringVar(&flagSshCmd, "cmd", "", "Remote command")
	sshFlags.BoolVar(&flagSshAgentForwarding, "agent-fwd", false, "SSH Agent Forwarding")

	destroyFlags := flag.NewFlagSet("destroy", flag.ExitOnError)
	destroyFlags.StringVar(&flagDir, "dir", "", "Environment directory")

	bbbMeetingFlags := flag.NewFlagSet("bbb-meeting", flag.ExitOnError)
	bbbMeetingFlags.StringVar(&flagConfig, "config", "", "YAML configuration")

	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s SUBCOMMAND -help\n", os.Args[0])
		fmt.Printf("\n")
		fmt.Printf("Available subcommands are:\n")
		fmt.Printf("- init:\n")
		fmt.Printf("  Create a new environment and VM in a new subdirectory.\n")
		fmt.Printf("\n")
		fmt.Printf("- ssh:\n")
		fmt.Printf("  Execute a remote command at the cloud VM via SSH.\n")
		fmt.Printf("\n")
		fmt.Printf("- destroy:\n")
		fmt.Printf("  Destroy the deployed environment.\n")
		fmt.Printf("\n")
		fmt.Printf("- bbb-meeting:\n")
		fmt.Printf("  Lists all active BigBlueButton meetings.\n")
		fmt.Printf("\n")
		os.Exit(1)
	}

	subcommands := map[string]struct {
		flagSet *flag.FlagSet
		main    func()
	}{
		"init":        {initFlags, initMain},
		"ssh":         {sshFlags, sshMain},
		"destroy":     {destroyFlags, destroyMain},
		"bbb-meeting": {bbbMeetingFlags, bbbMeetingMain},
	}

	subcommand, ok := subcommands[os.Args[1]]
	if !ok {
		fmt.Printf("%s: unknown subcommand %q\n", os.Args[0], os.Args[1])
		os.Exit(1)
	}

	err := subcommand.flagSet.Parse(os.Args[2:])
	if err != nil {
		panic(err)
	}

	if flagConfig != "" {
		configData, err := ioutil.ReadFile(flagConfig)
		if err != nil {
			panic(err)
		}
		config, err = cfg.Parse(configData)
		if err != nil {
			panic(err)
		}
	}

	subcommand.main()
}

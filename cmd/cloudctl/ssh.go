// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// ssh establishes a SSH connection to the VM and executes a command.

package main

import (
	"context"
	"fmt"
	"os"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cloud"
)

func sshMain() {
	sshHandler := &cloud.SshHandler{
		Dir:           flagDir,
		SshPrivateKey: config.Ssh.PrivateKey,
	}

	if flagSshCmd == "" {
		fmt.Printf("%s ssh: -cmd must be set\n", os.Args[0])
		os.Exit(1)
	}

	out, err := sshHandler.Exec(context.Background(), flagSshCmd, flagSshAgentForwarding)
	fmt.Printf("%s\n", out)
	if err != nil {
		panic(err)
	}
}

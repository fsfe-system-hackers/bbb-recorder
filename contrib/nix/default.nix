# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

{ config ? {}, overlays ? [] }:

let
  mkNixpkgs = { rev, sha256 }: import (builtins.fetchTarball {
    inherit sha256;
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  });
  nixpkgs = mkNixpkgs (import ./nixpkgs.nix);

  overlaysAll = [(import ./overlays.nix)] ++ overlays;
in nixpkgs { inherit config; overlays=overlaysAll; }

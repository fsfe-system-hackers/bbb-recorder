# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Revision of the nixpkgs as defined in the NixOS/nixpkgs repository.
# If in doubt, use the current HEAD of nixos-unstable-small.
{
  rev = "e10e367defcb64675f021e82cc9e0a0f4ec4f0b4";
  sha256 = "sha256-RFn4ZIA7/dW8Jjz1okEOCFjh3Hu1kHCR176A8NqCtV8=";
}

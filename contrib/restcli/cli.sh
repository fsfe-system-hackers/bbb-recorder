#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Simple CLI client for the RESTful^W RESTlike™ webapi. Honestly, this is just
# a wrapper around curl.

usage() { # : describes usage
  echo -e "Usage: $0 CMD [ARGS...]\n\nCMDs are:"
  sed -En 's/^([a-z_]+)\(\) \{ \# (.+)$/ \1 \2/p' "$0"
  echo -e "\nAPI's format is http://localhost:8080/api"
  echo -e "AUTH's format is user:pass"
  exit
}

logs() { # API AUTH [SINCE] : list important recent logging messages, optionally filtered SINCE
  [[ "$#" -ne "2" ]] && [[ "$#" -ne "3" ]] && usage
  curl -s -u "$2" -X GET -G --data-urlencode "since=${3}" --data-urlencode "no-get=true" "${1}/logs" | "$JQ"
}

logs_all() { # API AUTH [SINCE] : list all recent logging messages, optionally filtered SINCE
  [[ "$#" -ne "2" ]] && [[ "$#" -ne "3" ]] && usage
  curl -s -u "$2" -X GET -G --data-urlencode "since=${3}" "${1}/logs" | "$JQ"
}

bbb_meetings() { # API AUTH : lists active BBB meetings
  [[ "$#" -ne "2" ]] && usage
  curl -s -u "$2" -X GET "${1}/bbb/meetings" | "$JQ"
}

session_list() { # API AUTH : lists active recording sessions
  [[ "$#" -ne "2" ]] && usage
  curl -s -u "$2" -X GET "${1}/session/list" | "$JQ"
}

session_new() { # API AUTH BBB_MEETING_ID : create a new session for the BBB meeting
  [[ "$#" -ne "3" ]] && usage
  curl -s -u "$2" -X POST -d "{\"bbb_meeting_id\": \"${3}\"}" "${1}/session/new" | "$JQ"
}

session_destroy() { # API AUTH SESSION_ID : destroys a running session
  [[ "$#" -ne "3" ]] && usage
  curl -s -u "$2" -X POST -d "{\"session\": \"${3}\"}" "${1}/session/destroy" | "$JQ"
}

session_destroy_force() { # API AUTH SESSION_ID : destroys a running or failed session with force
  [[ "$#" -ne "3" ]] && usage
  curl -s -u "$2" -X POST -d "{\"session\": \"${3}\", \"force\": true}" "${1}/session/destroy" | "$JQ"
}

[[ -x "$(command -v jq)" ]] && JQ="jq" || JQ="cat"

[[ "$#" -eq "1" ]] && usage || cmd="$1"
[[ "$(type -t "$cmd")" != "function" ]] && usage || $cmd ${@:2}

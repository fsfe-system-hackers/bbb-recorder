# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

{ config ? {}
, overlays ? []
, pkgs ? import ./contrib/nix { inherit config overlays; }

, buildGoModule ? pkgs.buildGo118Module
}:

with pkgs;

buildGoModule rec {
  pname = "bbb-recorder";
  version = "unstable-git";

  src = lib.sources.cleanSource ./.;

  vendorSha256 = "sha256-FE51pi7iD96Iy0/CYSw0E9+pYzOQOcBg8Pj03NMCAmQ=";

  buildInputs = [ makeWrapper terraform ];

  preFixup = ''
    for f in $out/bin/*; do
      wrapProgram "$f" --prefix PATH : ${lib.makeBinPath [ terraform ]}
    done
  '';
}

// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

module git.fsfe.org/fsfe-system-hackers/bbb-recorder

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/oxzi/syscallset-go v0.1.0
	github.com/sethvargo/go-diceware v0.2.1
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/elastic/go-seccomp-bpf v1.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20220412020605-290c469a71a5 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
)

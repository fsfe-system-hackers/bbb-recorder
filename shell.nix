# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

{ config ? {}
, overlays ? []
, pkgs ? import ./contrib/nix { inherit config overlays; }
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    go_1_18
    golangci-lint
    jq
    nodePackages.jshint
    reuse
    shellcheck
    terraform
    yamllint
    yq-go
  ];
}

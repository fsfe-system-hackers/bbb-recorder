// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file implements a "forked" context, which copies relevant values from
// one context to a new one. The resulting context holds those values, but is
// not a child in the other context's hierarchy.

package webapi

import (
	"context"
	"time"
)

// forkedContext is a dummy implementation of the context.Context interface,
// similar to context.emptyCtx. However, our relevant key/value pairs are being
// copied. Constructed through the contextFork function.
type forkedContext struct {
	values map[any]any
}

func (_ forkedContext) Deadline() (time.Time, bool) {
	return time.Time{}, false
}

func (_ forkedContext) Done() <-chan struct{} {
	return nil
}

func (_ forkedContext) Err() error {
	return nil
}

func (ctx forkedContext) Value(key any) any {
	return ctx.values[key]
}

// contextFork based on a selected subset of variables. The new context will NOT
// be in a child relationship to the given context.
func contextFork(ctx context.Context) context.Context {
	pairs := make(map[any]any)
	for _, key := range []string{contextAuthUser, contextReqId} {
		pairs[key] = ctx.Value(key)
	}

	return forkedContext{values: pairs}
}

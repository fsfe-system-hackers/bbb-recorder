// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file implements the API endpoint to query BBB Meetings.

package webapi

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/bbb"
)

// bbbMeetingsHandler returns the current BigBlueButton meetings.
func (api *Api) bbbMeetingsHandler(rw http.ResponseWriter, r *http.Request) {
	ctx, ctxCancel := context.WithTimeout(r.Context(), 5*time.Second)
	defer ctxCancel()

	bbbApi := bbb.Api{
		Url:    api.config.Bbb.Url,
		Secret: api.config.Bbb.Secret,
	}
	meetings, err := bbbApi.GetMeetings(ctx)
	if err != nil {
		api.errorHandler(rw, r, err, http.StatusInternalServerError)
		return
	}

	_ = json.NewEncoder(rw).Encode(map[string]any{"meetings": meetings})
}

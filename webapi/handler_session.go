// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// This file contains web handlers to create, list, and delete recording
// sessions by instantiating and orchestrating cloud VMs.

package webapi

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/bbb"
	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cfg"
	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cloud"
)

// sessionListHandler lists all sessions, represented by their state file.
func (api *Api) sessionListHandler(rw http.ResponseWriter, r *http.Request) {
	sessions := make(map[string]*cloud.EnvironmentState)

	envDirs, err := os.ReadDir(api.config.StateDir)
	if err != nil {
		api.errorHandler(rw, r, err, http.StatusInternalServerError)
		return
	}

	for _, envDir := range envDirs {
		if !envDir.Type().IsDir() {
			continue
		}

		envState, err := cloud.NewEnvironmentState(filepath.Join(api.config.StateDir, envDir.Name()))
		if err != nil {
			api.log(r.Context(), "cannot create environment state for %s: %v", envDir, err)
			continue
		}
		sessions[filepath.Base(envDir.Name())] = envState
	}

	_ = json.NewEncoder(rw).Encode(map[string]any{"sessions": sessions})
}

// sessionNewHandler creates a new environment, tells the user and populates
// the session in the background.
func (api *Api) sessionNewHandler(rw http.ResponseWriter, r *http.Request) {
	var req struct {
		BbbMeetingId string `json:"bbb_meeting_id"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		api.errorHandler(rw, r, err, http.StatusInternalServerError)
		return
	}

	if req.BbbMeetingId == "" {
		api.errorHandler(rw, r, fmt.Errorf("empty bbb_meeting_id"), http.StatusInternalServerError)
		return
	}

	env := &cloud.Environment{
		HetznerCloudApiKey: api.config.HetznerCloudApiKey,
		SshPublicKeyNames:  api.config.Ssh.PublicKeyNames,
		BbbUrl:             api.config.Bbb.Url,
		BbbSecret:          api.config.Bbb.Secret,
		BbbMeetingId:       req.BbbMeetingId,
	}

	dir, err := env.Populate(api.config.StateDir)
	if err != nil {
		api.errorHandler(rw, r, err, http.StatusInternalServerError)
		return
	}

	api.log(r.Context(), "session for BBB meeting %q", req.BbbMeetingId)
	api.log(r.Context(), "to be stored in %v", dir)

	_ = json.NewEncoder(rw).Encode(map[string]any{"session": filepath.Base(dir)})

	go api.sessionCreate(contextFork(r.Context()), env)
}

// sessionCreate initiated from sessionNewHandler.
func (api *Api) sessionCreate(ctx context.Context, env *cloud.Environment) {
	envState, err := cloud.NewEnvironmentState(env.Dir)
	if err != nil {
		api.log(ctx, "cannot create environment state: %v", err)
		return
	}

	envState.Creator = ctx.Value(contextReqId).(string)
	envState.CreatedStart = time.Now()
	envState.MetaData["BBB Meeting ID"] = env.BbbMeetingId
	err = envState.Write()
	if err != nil {
		api.log(ctx, "cannot write environment state: %v", err)
		return
	}

	bbbMeetingNameChan := make(chan string, 1)
	go api.sessionCreateBbbName(ctx, env.BbbMeetingId, bbbMeetingNameChan)

	errLog := func(ctx context.Context, format string, args ...interface{}) {
		api.log(ctx, format, args...)
		envState.Error = fmt.Sprintf(format, args...)
		_ = envState.Write()
	}

	// Create and apply Terraform within the newly created environment.
	tf, err := env.Terraform()
	if err != nil {
		errLog(ctx, "cannot create terraform: %v", err)
		return
	}

	err = api.sessionCreateTf(ctx, tf)
	if err != nil {
		errLog(ctx, "creating terraform failed, %v", err)
		return
	}

	// Give the cloud provider some time, just because I love seeing random
	// delays within code.
	select {
	case <-ctx.Done():
		errLog(ctx, "waiting was denied, %v", ctx.Err())
		return
	case <-time.After(30 * time.Second):
	}

	// Check the server's status.
	for _, cmd := range api.config.UpCommands {
		err = api.sessionSshCmd(ctx, env.Dir, cmd)
		if err != nil {
			errLog(ctx, "checking upstarting VM failed, %v", err)
			return
		}
	}

	// Final delay to let the container start up..
	select {
	case <-ctx.Done():
		errLog(ctx, "waiting was denied, %v", ctx.Err())
		return
	case <-time.After(5 * time.Second):
	}

	select {
	case bbbMeetingName := <-bbbMeetingNameChan:
		envState.MetaData["BBB Meeting Name"] = bbbMeetingName
	case <-time.After(time.Second):
		// The go routine itself has already logged errors, nothing to do here.
	}

	envState.CreatedFinished = time.Now()
	err = envState.Write()
	if err != nil {
		errLog(ctx, "cannot update environment state: %v", err)
		return
	}

	api.log(ctx, "session was created successfully")
}

// sessionCreateBbbName fetches the human readable BBB session name, to be
// stored in the meta data.
func (api *Api) sessionCreateBbbName(ctx context.Context, meetingId string, ch chan string) {
	ctxBbb, ctxBbbCancel := context.WithTimeout(ctx, 5*time.Second)
	defer ctxBbbCancel()

	bbbApi := bbb.Api{
		Url:    api.config.Bbb.Url,
		Secret: api.config.Bbb.Secret,
	}
	meetings, err := bbbApi.GetMeetings(ctxBbb)
	if err != nil {
		api.log(ctxBbb, "cannot fetch BBB meetings: %v", err)
		ch <- ""
	}

	for name, id := range meetings {
		if id == meetingId {
			ch <- name
			return
		}
	}

	api.log(ctxBbb, "cannot find BBB meeting for requested id %q", meetingId)
	ch <- ""
}

// sessionCreateTf applies Terraform to the new environment in the background.
func (api *Api) sessionCreateTf(ctx context.Context, tf *cloud.Terraform) error {
	steps := []struct {
		name string
		f    func(context.Context) ([]string, error)
	}{
		{"init ", tf.Init},
		{"plan ", tf.Plan},
		{"apply", tf.Apply},
	}

	for _, step := range steps {
		api.log(ctx, "starting terraform %s", step.name)

		stepCtx, stepCtxCancel := context.WithTimeout(ctx, time.Minute)
		defer stepCtxCancel()

		out, err := step.f(stepCtx)
		if err != nil {
			for _, l := range out {
				api.log(ctx, "terraform %s > %s", step.name, l)
			}
			return fmt.Errorf("step %s failed: %v", step.name, err)
		}

		api.log(ctx, "finished terraform %s", step.name)
	}

	return nil
}

// sessionDestroyHandler is the endpoint to start destroying a session.
func (api *Api) sessionDestroyHandler(rw http.ResponseWriter, r *http.Request) {
	var req struct {
		Session string `json:"session"`
		Force   bool   `json:"force"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		api.errorHandler(rw, r, err, http.StatusInternalServerError)
		return
	}

	envDir := filepath.Join(api.config.StateDir, filepath.Clean(req.Session))
	sessionStat, sessionStatErr := os.Stat(envDir)
	if sessionStatErr != nil {
		api.errorHandler(rw, r, sessionStatErr, http.StatusInternalServerError)
		return
	} else if !sessionStat.IsDir() {
		api.errorHandler(rw, r, fmt.Errorf("no session directory"), http.StatusInternalServerError)
		return
	}

	api.log(r.Context(), "session %q / %q will be destroyed", req.Session, envDir)
	api.log(r.Context(), "force destruction: %t", req.Force)

	_ = json.NewEncoder(rw).Encode(req)

	go api.sessionDestroy(contextFork(r.Context()), envDir, req.Force)
}

// sessionDestroy is being executed forked off from sessionDestroyHandler.
func (api *Api) sessionDestroy(ctx context.Context, envDir string, forceDestruction bool) {
	envState, err := cloud.NewEnvironmentState(envDir)
	if err != nil {
		api.log(ctx, "cannot create environment state: %v", err)
		return
	}

	if envState.Destroyer != "" && !forceDestruction {
		api.log(ctx, "environment state was already destroyed, %v", envState)
		return
	}

	envState.Destroyer = ctx.Value(contextReqId).(string)
	envState.DestroyedStart = time.Now()
	err = envState.Write()
	if err != nil {
		api.log(ctx, "cannot write environment state: %v", err)
		return
	}

	errLog := func(ctx context.Context, format string, args ...interface{}) {
		api.log(ctx, format, args...)
		envState.Error = fmt.Sprintf(format, args...)
		_ = envState.Write()
	}

	// Execute tear down commands
	for _, cmd := range api.config.DownCommands {
		err = api.sessionSshCmd(ctx, envDir, cmd)
		if err != nil {
			errLog(ctx, "down command failed, %v", err)
			if !forceDestruction {
				return
			}
			errLog(ctx, "skipping abortion as force destruction is used")
		}
	}

	// Terraform destroy
	api.log(ctx, "starting terraform destroy")

	tf := &cloud.Terraform{Dir: envDir}

	stepCtx, stepCtxCancel := context.WithTimeout(ctx, time.Minute)
	defer stepCtxCancel()

	tfOut, err := tf.Destroy(stepCtx)
	if err != nil {
		for _, l := range tfOut {
			api.log(ctx, "terraform destroy > %s", l)
		}

		errLog(ctx, "terraform destroy failed: %v", err)
		if !forceDestruction {
			return
		}
		errLog(ctx, "skipping abortion as force destruction is used")
	}

	api.log(ctx, "finished terraform destroy")

	envState.DestroyedFinished = time.Now()
	err = envState.Write()
	if err != nil {
		errLog(ctx, "cannot update environment state: %v", err)
		return
	}

	api.log(ctx, "session was destroyed successfully")
}

// sessionSshCmd executes a cfg.Command via SSH.
func (api *Api) sessionSshCmd(ctx context.Context, dir string, cmd cfg.Command) error {
	sshHandler := &cloud.SshHandler{
		Dir:           dir,
		SshPrivateKey: api.config.Ssh.PrivateKey,
	}

	api.log(ctx, "starting remote execution of %v", cmd)

	timeoutCtx, timeoutCtxCancel := context.WithTimeout(ctx, cmd.Timeout)
	defer timeoutCtxCancel()

	sshOut, err := sshHandler.Exec(timeoutCtx, cmd.Cmd, cmd.AgentForwarding)
	if err != nil {
		for _, l := range strings.Split(string(sshOut), "\n") {
			api.log(ctx, "%q > %s", cmd.Cmd, l)
		}
		return fmt.Errorf("%v failed, %v", cmd, err)
	}

	api.log(ctx, "successfully executed %v", cmd)

	return nil
}

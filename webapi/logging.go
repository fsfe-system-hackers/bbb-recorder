// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package webapi

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

// LogEvent created by an API event.
type LogEvent struct {
	Timestamp time.Time
	RequestId string
	Message   string
}

func (event LogEvent) String() string {
	return fmt.Sprintf("[%s] [%s] %s",
		event.Timestamp.Format(time.UnixDate), event.RequestId, event.Message)
}

// loggerSize defines the amount of temporary hold messages in the buffer.
const loggerSize = 512

// log the request to the logger with a prefixed request ID.
func (api *Api) log(ctx context.Context, format string, args ...interface{}) {
	api.bufferMutex.Lock()
	defer api.bufferMutex.Unlock()

	event := LogEvent{
		Timestamp: time.Now(),
		RequestId: ctx.Value(contextReqId).(string),
		Message:   fmt.Sprintf(format, args...),
	}

	api.bufferPos = (api.bufferPos + 1) % loggerSize
	api.buffer[api.bufferPos] = event

	_, _ = api.bufferWriter.WriteString(event.String() + "\n")
	_ = api.bufferWriter.Flush()
}

// loggerEvents from the logger's buffer in the correct order - oldest to latest.
func (api *Api) loggerEvents() []LogEvent {
	api.bufferMutex.RLock()
	defer api.bufferMutex.RUnlock()

	events := append(api.buffer[api.bufferPos+1:loggerSize], api.buffer[0:api.bufferPos+1]...)

	// Remove empty leading entries at newer position which were not used yet.
	startPos := loggerSize
	for i, event := range events {
		if event != (LogEvent{}) {
			startPos = i
			break
		}
	}

	return events[startPos:loggerSize]
}

// loggerHandler serves the most recent logs. By passing a timestamp as the
// optional `since` GET parameter, only newer log entries will be returned.
// By passing the optional `no-get` parameter, the GET requests to the API
// will be excluded.
func (api *Api) loggerHandler(rw http.ResponseWriter, r *http.Request) {
	events := api.loggerEvents()

	sinceReq := r.URL.Query().Get("since")
	if sinceReq != "" {
		since, err := time.Parse(time.RFC3339, sinceReq)
		if err != nil {
			api.errorHandler(rw, r, err, http.StatusInternalServerError)
			return
		}

		startPos := 0
		for i, event := range events {
			if event.Timestamp.After(since) {
				startPos = i
				break
			}
		}

		events = events[startPos:len(events)]
	}

	noGetReq := r.URL.Query().Get("no-get")
	if noGetReq != "" && noGetReq != "false" && noGetReq != "0" {
		msgFilter := regexp.MustCompile(`^\w+ GET-requested \/api\/.+$`)

		for i := len(events) - 1; i >= 0; i-- {
			if msgFilter.MatchString(events[i].Message) {
				events = append(events[:i], events[i+1:]...)
			}
		}
	}

	_ = json.NewEncoder(rw).Encode(events)
}

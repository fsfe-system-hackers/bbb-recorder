// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Package webapi implements a usable RESTful API.
package webapi

import (
	"bufio"
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	"github.com/sethvargo/go-diceware/diceware"

	"git.fsfe.org/fsfe-system-hackers/bbb-recorder/cfg"
)

const (
	// contextReqId is the key for the request ID, passed via context.
	contextReqId = "reqid"

	// contextAuthUser is the key for the authorized user, passed via context.
	contextAuthUser = "authuser"
)

// Api to be used in a RESTful way to manage cloud VMs for recording.
type Api struct {
	config cfg.Config
	router *mux.Router

	buffer       []LogEvent
	bufferPos    int
	bufferWriter *bufio.Writer
	bufferMutex  sync.RWMutex
}

// NewApi based on the given configuration.
func NewApi(config cfg.Config) (*Api, error) {
	logF, err := os.OpenFile(config.LogFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return nil, err
	}

	api := &Api{
		config: config,
		router: mux.NewRouter(),

		buffer:       make([]LogEvent, loggerSize),
		bufferPos:    loggerSize - 1,
		bufferWriter: bufio.NewWriter(logF),
	}

	api.router.Use(mux.CORSMethodMiddleware(api.router))
	api.router.Use(api.jsonContentMiddleware)
	api.router.Use(api.basicAuthMiddleware)
	api.router.Use(api.requestIdMiddleware)
	api.router.Use(api.loggingMiddleware)

	api.router.HandleFunc("/api/logs", api.loggerHandler).Methods("GET")
	api.router.HandleFunc("/api/bbb/meetings", api.bbbMeetingsHandler).Methods("GET")
	api.router.HandleFunc("/api/session/list", api.sessionListHandler).Methods("GET")
	api.router.HandleFunc("/api/session/new", api.sessionNewHandler).Methods("POST")
	api.router.HandleFunc("/api/session/destroy", api.sessionDestroyHandler).Methods("POST")

	return api, nil
}

// jsonContentMiddleware just sets the Content-Type to application/json.
func (api *Api) jsonContentMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")

		next.ServeHTTP(rw, r)
	})
}

// basicAuthMiddleware verifies the HTTP Basic Authentication header against the
// allowed users in the config.
func (api *Api) basicAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		name, pass, ok := r.BasicAuth()
		if ok {
			for _, authUser := range api.config.AuthUsers {
				if authUser.Name != name || authUser.Pass != pass {
					continue
				}

				r = r.WithContext(context.WithValue(r.Context(), contextAuthUser, name))
				next.ServeHTTP(rw, r)
				return
			}
		}

		rw.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
		rw.WriteHeader(http.StatusUnauthorized)

		_ = json.NewEncoder(rw).Encode(map[string]string{
			"error": "basic authentication required",
		})
	})
}

// requestIdMiddleware added to the request's context to identify hierarchies.
func (api *Api) requestIdMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		words, _ := diceware.GenerateWithWordList(3, diceware.WordListOriginal())
		reqId := strings.Join(words, "-")
		r = r.WithContext(context.WithValue(r.Context(), contextReqId, reqId))

		next.ServeHTTP(rw, r)
	})
}

// loggingMiddleware logs incoming requests.
func (api *Api) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		api.log(ctx, "%s %s-requested %s", ctx.Value(contextAuthUser), r.Method, r.RequestURI)

		next.ServeHTTP(rw, r)
	})
}

// errorHandler to be called in case of an error which needs to both cancel the
// request as well as being logged.
func (api *Api) errorHandler(rw http.ResponseWriter, r *http.Request, err error, status int) {
	api.log(r.Context(), "request error: %v", err)

	rw.WriteHeader(status)
	_ = json.NewEncoder(rw).Encode(map[string]string{"error": err.Error()})
}

// ServeHTTP implements the http.Handler interface, allowing an Api being used
// within a Go http.Server.
func (api *Api) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	api.router.ServeHTTP(rw, r)
}
